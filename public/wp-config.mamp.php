<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'F1_jhu');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         ';i6YNdQs}{I<OR^}e]{FX!%xf.,9,:`iPI{<9_:-q:a>%9#[eep?Iw#A8w5wT;wo');
define('SECURE_AUTH_KEY',  'k$E_ev*)Gts1cU2rQm-#y,vv}sIHu%;ji(}hb3DqmE2N^Fk_J0k9|W&^)*eNqC3p');
define('LOGGED_IN_KEY',    '2XTlx_Na#9f9Bg=DZ!tV^}pux{ ~TSpvs.vH)ZI>rzCLW+L/))<NW?TVz1UyAAF1');
define('NONCE_KEY',        '0 1jfxP516q]9SX{g6q*cc7OL8~^4_=)5Zm[PjJ}_9TM;x>;B-^Xgx->-_L4PU7M');
define('AUTH_SALT',        'L5-@lG%Usps>h0XnLIrTg 0X(ljTh$@hlRd! U#L+5q%|>zIXOzr[eZhvd2>?~0o');
define('SECURE_AUTH_SALT', '}Rg?_*L ?dOp3oqU>!gO%P:fZ/,m/pL?xHv$0p7h%c=qp3[y&:$=?{NfxvXyx$m_');
define('LOGGED_IN_SALT',   ')#i{FM;+<yb:-7>qy,``0}mn0lI@QBqGg8|Sex=~0I[RNog:Tx Bs,tU=5~5Cdd<');
define('NONCE_SALT',       'hN=-`3N1/l)-LLc!p[r^;fgt<m$__9 Ee@^iBWR6%slg[{DyQ<^v.,V7!g_n1nmt');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
 define('WP_DEBUG', true);
 define( 'WP_DEBUG_LOG', true );
 define( 'WP_DEBUG_DISPLAY', true );

define('WP_HOME','http://localhost:8081');
define('WP_SITEURL','http://localhost:8081');

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
