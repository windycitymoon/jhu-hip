<?php

$context = Timber::get_context();
$post = new TimberPost();
$context['post'] = $post;

$advisors_args = array(
  'post_type' => 'advisors',
  'posts_per_page' => -1,
  'meta_key' => 'advisor_last_name',
  'orderby' => 'meta_value',
  'order' => 'ASC'
);

$context['advisors'] = Timber::get_posts($advisors_args);
Timber::render( array( 'page-' . $post->post_name . '.twig', 'page-' . $post->ID . '.twig', 'page.twig' ), $context );
