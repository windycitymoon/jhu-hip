jQuery(document).ready(function($) {

  'use strict';

  // Create the dropdown base
  $("<select />").prependTo(".briefs-detail__wrapper");

  // Create default option "Go to..."
  $("<option />", {
    "selected": "selected",
    "value": "",
    "text": "Select section...",
    "class": "nav__link"
  }).appendTo(".briefs-detail__wrapper select");

  // Populate dropdown with menu items
  $(".nav--briefs-detail .nav__link").each(function() {
    var el = $(this);
    $("<option />", {
      "value": el.attr("href"),
      "text": el.text(),
      "class": "nav__link"
    }).appendTo(".briefs-detail__wrapper select");
  });

  // //scroll2id
  $(".nav--briefs-detail .nav__link, .hero__link a[href*='#']").mPageScroll2id();


  //Highlight active menu item
  $('.nav--briefs-detail .nav__item').on('click', function(e){
    e.preventDefault;
    $('.nav--briefs-detail .nav__item').removeClass("active");
    $(this).addClass("active");
  });

  $(".briefs-detail__wrapper select").addClass('briefs__mobile-menu');

  // FIX MENU AFTER SCROLL FROM HEADER
  var briefs_mobile_menu = $(".briefs__mobile-menu");
  $(window).scroll(function() {
        var scroll = $(window).scrollTop();
        if (scroll >= 225) {
            briefs_mobile_menu.addClass("is-fixed");
        } else {
            briefs_mobile_menu.removeClass("is-fixed");
        }
    });

});
