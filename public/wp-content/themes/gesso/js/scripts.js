// Custom scripts file

jQuery( document ).ready(function( $ ) {

  'use strict';

  //Searchbox
  $('.searchbox__link').on('click', function(){
    $(this).toggleClass('is-open');
    $('.searchbox__dropdown').slideToggle();
  });

  //Fixed Menu options add class
  $(".briefs__mobile-menu").change(function() {
    window.location = $(this).find("option:selected").val();
  });

  //Fixed Briefs Menu
  $(".l-sidebars__sidebar-inner").fixTo(".l-sidebars__sidebar");
  // $(".briefs__mobile-menu").fixTo(".l-sidebars__sidebar", {
  //   zIndex: 40
  // });

  //Stackable Table on Briefs Detail Pages
  $('.briefs-table__table').stacktable();

  //Open all external briefs detail links in external window.
  $('.briefs-detail__wrapper a[href*="http"]').each(function() {
    $(this).attr('target', '_blank');
  });

  // Generic function that runs on window resize.
  function resizeStuff() {
  }

  // Runs function once on window resize.
  var TO = false;
  $(window).resize(function () {
    if (TO !== false) {
      clearTimeout(TO);
    }

    // 200 is time in miliseconds.
    TO = setTimeout(resizeStuff, 200);
  }).resize();

});
