<?php
/*
 * Template Name: Homepage Template
 * Description: A Page Template for the HomePage.
 */

$context = Timber::get_context();
$post = new TimberPost();

$partners_args = array(
  'post_type' => 'partners',
  'posts_per_page' => -1,
  'orderby' => 'title',
  'order' => 'DESC'
);

$context['repeater'] = new TimberPost(get_field('homepage_briefs_selection'));
$context['post'] = $post;
$context['partners'] = Timber::get_posts($partners_args);
Timber::render( array( 'front-page.twig', 'page.twig' ), $context );
