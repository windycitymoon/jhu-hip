<?php
/**
 * Template Name: Sortable Landing (Briefs)
 * Briefs Landing Page - All Briefs
*/

$context = Timber::get_context();
$post = new TimberPost();
$context['post'] = $post;
// Define generic templates.
$templates = array(
	'page-' . $post->post_name . '.twig', 
	'page-' . $post->ID . '.twig', 
	'page.twig' 
	);

// Get all selected and ordered Briefs in a WP_Query fashion.
$brief_args = array(
	'post_type' => 'briefs',
	'post__in' => $post->all_briefs,
	'orderby' => 'post__in',
	'posts_per_page' => -1,
	'facetwp' => true,
	);
$wp_query_briefs = new WP_Query( $brief_args );

// Covert WP_Query post objects into TimberPost objects.
$context['post']->all_briefs = Timber::get_posts( $wp_query_briefs );

// Render twig template.
Timber::render( $templates, $context );
