<?php

$context = Timber::get_context();
$post = new TimberPost();

$partners_args = array(
  'post_type' => 'partners',
  'posts_per_page' => -1,
  'orderby' => 'title',
  'order' => 'ASC'
);

$context['partners'] = Timber::get_posts($partners_args);
$context['post'] = $post;
Timber::render( array( 'page-' . $post->post_name . '.twig', 'page-' . $post->ID . '.twig', 'page.twig' ), $context );
