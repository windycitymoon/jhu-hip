<?php
/*
Plugin Name: F1 JHU HIP Custom
Description: Custom functions, filters and action hooks.
Author: Forum One
Author URI: https://forumone.com
*/
// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}


/**
 * Sets a custom location for acf-json folder out of the theme directory.
 * https://www.advancedcustomfields.com/resources/local-json/
 * https://www.advancedcustomfields.com/resources/acf-settings/
 * @param string $path
 * @return string
 */
function f1__acf_local_json_dir( $path ) {
    // Set custom path in /wp-content/acf-json
	$_path = WP_CONTENT_DIR . '/acf-json';

	if ( is_array( $path ) ) {
        // If $path is an array then this the load_json filter settings.
		unset( $path[0] );
		$path[] = $_path;
	} else {
        // If $path is a string then this the save_json filter settings.
		$path = $_path;
	}

    // return
	return $path;
}
add_filter( 'acf/settings/save_json', 'f1__acf_local_json_dir' );
add_filter( 'acf/settings/load_json', 'f1__acf_local_json_dir' );
